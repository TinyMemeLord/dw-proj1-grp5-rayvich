Creating and Storing Academic Projects 

INTRODUCTION

This Web Application lets the user fill out a form describing an academic project with fields such as number of hours and owner.
It validates the data, showing useful feedback to the user prompting them to fill out the form correctly and not allowing them to add a project otherwise.
After validating the data it renders the project for the user. The user can edit the project from there.
The user can also manipulate the data within the local storage by saving or appending to it, clearing it and loading from it.
The status bar displays useful messages to the user, and the user can use the search bar to find projects that match what they are looking for.

HOW TO RUN

Simply fill out the form and click the appropriate buttons. The program will give you feedback which you should follow.

CREDITS

Maintainers: Maxime Rayvich, Shay Alex Lelichev
Organization: Dawson College, Computer Science Department, Web Development 2, Section 4

LICENSE

MIT License

Copyright (c) 2021 Maxime Rayvich, Shay Alex Lelichev

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

