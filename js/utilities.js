'use strict'


/**
 * -Maxime Rayvich
 * Checks to see if the  input field that triggered this event respects it's corresponding regex pattern.
 * Sets it's corresponding object in the regex_patterns_confirmed global variable to be a boolean value depending if it's value matched the regex.
 * Calls other functions to give visual feedback.
 */

function validate(){
    let inputs = document.querySelectorAll('.top_input');
   // sets index by finding where 'this' is located 
    let index = -1;
    let id = this.id;
    for(let i = 0; i < inputs.length; i++){
        if(inputs[i].id === this.id){
            index=i;
        }
    }
    let input_value = this.value;
    let pattern = regex_patterns[index].pattern;
    let check = pattern.test(input_value);
    regex_patterns_confirmed[index].valid = check;
    give_user_feedback(index);
    validateAll();
}

/**
 * -Maxime Rayvich
 * Takes as input a number representing the index of the input field.
 * Checks if the corresponding global variable's index validity field is set to true or false.
 * Calls two functions, one with text feedback and one with image feedback. Params correspond to whether or not that field is valid.
 * @param {number} index 
 */
function give_user_feedback(index){
    let input_fields = document.querySelectorAll('.wrong_or_right');
    let input_field = input_fields[index];
    let labels = document.querySelectorAll('label');
    let name = labels[index].textContent;
   // let feedback = appropriate_feedback(index);
    if(regex_patterns_confirmed[index].valid == false){
        set_feedback_text(input_field,'red','white',`wrong format for ${name}${appropriate_feedback(index)}`)
        set_feedback_icon(index, false);
    }
    else{
        set_feedback_text(input_field, '#f5f5f5','black',"" );
        set_feedback_icon(index, true);
    }
}
/**
 * -Maxime Rayvich
 * @param {number} index
 * @returns string positioned at index
 */
function appropriate_feedback(index){
    let feedback_array = 
    [" Has to be between 2 and 4 characters", 
    " Has to be between 1 and 60 characters and start with an upper case",
    " Has to be between 2 and 50 characters",
    " You have to choose a cetegory",
    " Has to be a number, at least 0",
    " Has to be a number, at least 0",
    " You have to choose a status",
    " Maximum of 200 characters"   
];
return feedback_array[index];
}
/**
 * -Maxime Rayvich
 * Sets those attributes accordingly.
 * @param {Object} input_field Takes as input an object in the dom
 * @param {String} background background
 * @param {String} color color
 * @param {String} content textContent
 */
function set_feedback_text(input_field, background, color, content){
    input_field.style.background=background;
    input_field.style.color = color;
    input_field.textContent=content;
}
/**
 * -Maxime Rayvich
 * Sets 'X' icon next to that field if the validity is false, and checkmark icon if it is true by calling another function.
 * @param {number} index Takes as input a number representing the index of the input field
 * @param {boolean} checker A boolean representing the validity of that fields.
 */
function set_feedback_icon(index, validity){
   let img_containers = document.querySelectorAll('.svg_container');
   let img=img_containers[index];
   let labels = document.querySelectorAll('label');
   let id_name = labels[index].textContent;
   let checkId = document.getElementById(id_name);
   let class_name = "validity_icons";
   if(checkId != null){
       checkId.remove();
   }
   let checkIcon = document.querySelector('#icon');
   if(checkIcon != null){
       checkIcon.remove();
   }
   if (validity == true){
       appendItem("img","../images/check-solid.svg","check-mark",id_name,'5vw','5vh',img, class_name);
    }
    else{
        appendItem("img", "../images/times-circle-solid.svg", "X-mark", id_name, '5vw','5vh',img, class_name);
    }
}
/**
 * -Maxime Rayvich
 * Creates element with given parameters and appends it to given parent.
 * @param {String} elem 
 * @param {String} src 
 * @param {String} alt 
 * @param {String} id_name 
 * @param {String} width 
 * @param {String} height 
 * @param {object} parent 
 */
function appendItem(elem, src, alt, id_name, width, height,parent,class_name){
    let icon = document.createElement(elem);
    icon.src=src;
    icon.alt=alt;
    icon.id=id_name;
    icon.className=class_name;
    icon.style.width=width;
    icon.style.height=height;
    parent.append(icon);
}
/**
 * -Maxime Rayvich
 * Checks if all patterns are confirmed to be 'true'
 * If they are, calls enable_disable_button with the ID of the add button and value of false
 * This sets that button's disabled value to be false
 * Otherwise set it to true.
 */
function validateAll(){
    let checker = 0;    
    for(let k of Object.values(regex_patterns_confirmed)){
        if(k.valid == true){
            checker++;
        }
    }

        if (checker == regex_patterns_confirmed.length){
            enable_disable_button('#add', false);
        }
        else{
            enable_disable_button('#add', true);
        }
    }

/**
 * -Maxime Rayvich
 * Sets the value of that buton's disabled attribute to that boolean.
 * @param {String} buttonID Takes as input a String with the id of a button
 * @param {Boolean} disabled Takes as input a boolean
 */
function enable_disable_button(buttonID, disabled){
    let button = document.querySelector(buttonID);
    button.disabled = disabled;
    }

    
/**
 * -Maxime Rayvich
 * Gets called when the reset button is clicked
 * Resets all input fields to their default values.
 */
function clear_form(){
    let inputs = document.querySelectorAll('input');
    for(let i of inputs){
        i.value="";
    }
    let select = document.querySelectorAll("select");
    for(let i of select){
    i.options[0].selected = true;
    }
   let desc = document.querySelector("#short_description");
   desc.value="";
   enable_disable_button("#add", true);
   reset_regex_patterns_confirmed();
   reset_icons();
   
}

/**
 * -Maxime Rayvich
 * Resets the global variable regex_patterns_confirmed to its default
 */

function reset_regex_patterns_confirmed(){
    regex_patterns_confirmed=[{input:'proj',valid: false},
    {input:'name',valid: false},
    {input:'title',valid: false},
    {input:'category',valid:false},
    {input:'hours',valid:false},
    {input:'rate',valid:false},
    {input:'status',valid:false},
    {input:'desc', valid: true}
];
}

/**
 * -Maxime Rayvich
 * Removes all the checkmark and x mark icons
 */
function reset_icons(){
    let icons = document.querySelectorAll('.validity_icons');
    for(let i of icons){
        i.remove();
    }
}

/**
 * -Shay Alex Lelichev
 * This function assigns event listeners to all of the delete buttons.
 */
function makeDelete() {
    let deleters = document.querySelectorAll(".delete");
    deleters = deleters[deleters.length-1];
    deleters.addEventListener('click' , () => {
        deleteTableRow(deleters);
        sessionStorage.setItem('currentSession' , JSON.stringify(projArray));
    });
}

/**
 * -Shay Alex Lelichev
 * This function assigns event listeners to all of the edit buttons.
 */
function makeEdit() {
    let edits = document.querySelectorAll('.edit');
    edits = edits[edits.length-1];
    edits.addEventListener('click' , () => {
        editTableRow(edits);
        sessionStorage.setItem('currentSession' , JSON.stringify(projArray));
    });
}

/**
 * -Shay Alex Lelichev
 * This function checks if some conditions are met in order to enable the write button.
 */
function checkWriteButton() {
    if (projArray.length == 0 || localStorage.getItem('savedProjects') == JSON.stringify(projArray)) {
        enable_disable_button('#localWrite' , true);
    } else {
        enable_disable_button('#localWrite' , false);
    }
}