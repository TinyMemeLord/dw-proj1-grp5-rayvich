'use strict'

/**
 * -Shay Alex Lelichev
 * accepts an array and returns an object with that array's values as object values.
 * @param {Array} input array containing all of the fields to be turned into an object.
 * @returns {Object} object containing all of the inputs.
 */
function createProjectObject(input) {
    let returnObj = {
        id: input[0].value,
        name: input[1].value,
        title: input[2].value,
        category: input[3].value,
        hours: input[4].value,
        rate: input[5].value,
        status: input[6].value,
        descr: input[7].value
                    };
    return returnObj;
}
/**
 * -Shay Alex Lelichev
 * This function gets an object and an index and uses them to create a table.
 * @param {Object} projObj The object that is put in the table.
 * @param {number} rowIndex The index of the row that the object is going to be displayed in.
 */
function updateProjectsTable(projObj , rowIndex) {
    let table = document.querySelector('#tableRender');
    let row = table.insertRow(rowIndex);
    let cell;
    let inputValues = Object.values(projObj);
    for (let i = 0; i < inputValues.length; i++) {
        cell = row.insertCell(i);
        cell.innerHTML = `${inputValues[i]}`;
    }
    cell = row.insertCell(8);
    cell.innerHTML = `<image class="edit" src="../images/edit-regular.svg" alt="edit icon" width="16px" height="16px">`;
    cell = row.insertCell(9);
    cell.innerHTML = `<image class="delete" src="../images/trash-alt-regular.svg" alt="trash icon" width="16px" height="16px">`;
    if (rowIndex % 2 !== 0) {
        for (let i = 0; i < 10; i++) {
            document.querySelector('#tableRender').rows[rowIndex].cells[i].style.backgroundColor = '#ababab';
        }
    }
    enable_disable_button('#localWrite' , false);
    enable_disable_button('#localRead' , false);
}

/**
 * -Shay Alex Lelichev
 * This function accepts an element and uses it to delete the row that contains this element and remove the 
 * project associated with this row from the projArray.
 * @param {HTMLElement} elem Element that is in the row of the table to be deleted.
 */
function deleteTableRow(elem) {
    if (confirm("Are you sure you want to delete this project?")) {
        let i = elem.parentElement.parentElement.rowIndex;
        document.querySelector('#tableRender').deleteRow(i);
        if (projArray[1] == undefined) {
            projArray = [];
        } else {
            let newProjArray = projArray.slice(i);
            for (let j = 0; j < newProjArray.length + 1; j++) {
                projArray.pop();
            }
            projArray = projArray.concat(newProjArray);
        }
        enable_disable_button('#localWrite' , false);
        enable_disable_button('#localRead' , false);
        alert("Project removed.");
    } else {
        alert("The project has not been removed.");
    } 
}

/**
 * -Shay Alex Lelichev
 * This function takes an element and makes it so that you can edit the project that is represented in that row
 * you can then save the project by clicking on another element and it updates it in the local storage and ProjArray.
 * @param {HTMLElement} elem element that is in the row that is to be edited.
 */
function editTableRow(elem) {
    let i = elem.parentElement.parentElement.rowIndex;
    elem.parentElement.innerHTML = `<image class="save" src="../images/save-solid.svg" alt="save icon" width="16px" height="16px">`;
    let savers = document.querySelectorAll(".save");
        savers = savers[savers.length-1];
    let cellValue;
    for (let j = 0; j < 8; j++) {
        cellValue = document.querySelector('#tableRender').rows[i].cells[j].innerHTML;
        document.querySelector('#tableRender').rows[i].cells[j].innerHTML = `<input type="text" value="${cellValue}">`
    }
    savers.addEventListener('click' , () => {
        let newCell;
        let ValuesArray = Object.values(projArray[i - 1]);
        for (let j = 0; j < 8; j++) {
            newCell = document.querySelector('#tableRender').rows[i].cells[j];
            ValuesArray[j] = newCell.childNodes[0].value;
            newCell.innerHTML = ValuesArray[j];
        }
        let newObj = {
            id: ValuesArray[0],
            name: ValuesArray[1],
            title: ValuesArray[2],
            category: ValuesArray[3],
            hours: ValuesArray[4],
            rate: ValuesArray[5],
            status: ValuesArray[6],
            descr: ValuesArray[7]
        }
        let newProjArray = projArray.slice(i);
        for (let j = 0; j < newProjArray.length + 1; j++) {
            projArray.pop();
        }
        projArray.push(newObj);
        projArray = projArray.concat(newProjArray);
        sessionStorage.setItem('currentSession' , JSON.stringify(projArray));
        enable_disable_button('#localWrite' , false);
        elem = document.querySelector('#tableRender').rows[i].cells[8];
        elem.innerHTML = `<image class="edit" src="../images/edit-regular.svg" alt="edit icon" width="16px" height="16px">`;
        makeEdit();
    });
}

/**
 * -Shay Alex Lelichev
 * this function saves all of the current projects displayed on the table to the local storage and enables/disables some buttons.
 */
function saveAllProjects2Storage() {
    localStorage.setItem('savedProjects' , JSON.stringify(projArray));
    document.querySelector('#generatedTable').childNodes[1].innerHTML = "All projects have been saved to local storage."
    enable_disable_button('#localWrite' , true);
    enable_disable_button('#localRead' , false);
    enable_disable_button('#localClear' , false);
    enable_disable_button('#localAppend' , false);
}

/**
 * -Shay Alex Lelichev
 * This function appends all of the current projects to the local storage and enables/disables some buttons.
 */
function appendAllProjects2Storage() {
    localStorage.setItem('savedProjects' , JSON.stringify(JSON.parse(localStorage.getItem('savedProjects')).concat(projArray)));
    document.querySelector('#generatedTable').childNodes[1].innerHTML = "All projects have been appended to local storage."
    checkWriteButton();
    enable_disable_button('#localRead' , false);
    enable_disable_button('#localClear' , false);
}

/**
 * -Shay Alex Lelichev
 * This function clears all of local storage and enables/disables some buttons.
 */
function clearAllProjectsFromStorage() {
    localStorage.setItem('savedProjects' , JSON.stringify([]));
    document.querySelector('#generatedTable').childNodes[1].innerHTML = "All projects have been removed from local storage."
    enable_disable_button('#localWrite' , false);
    enable_disable_button('#localRead' , false);
    enable_disable_button('#localClear' , true);
}

/**
 * -Shay Alex Lelichev
 * This function reads all of the current projects from the local storage, represents them on the table, and enables/disables some buttons.
 */
function readAllProjectsFromStorage() {
    let readProjArray = JSON.parse(localStorage.getItem('savedProjects'));
    document.querySelector('#generatedTable').childNodes[1].innerHTML = `There are ${JSON.parse(localStorage.getItem('savedProjects')).length} projects in local storage.`
    let table = document.querySelector('#tableRender');
    for (let i = 1; i < table.rows.length; i) {
        table.deleteRow(1);
    }
    for (let j = 0; j < readProjArray.length; j++) {
        updateProjectsTable(readProjArray[j] , j+1);
        makeDelete();
        makeEdit();
    }
    projArray = readProjArray;
    sessionStorage.setItem('currentSession' , JSON.stringify(projArray));
    enable_disable_button('#localWrite' , true);
    enable_disable_button('#localRead' , true);
    if (localStorage.getItem('savedProjects') == "[]") {
        enable_disable_button('#localAppend' , true);
        enable_disable_button('#localClear' , true);
    }
}

/**
 * -Shay Alex Lelichev
 * This function compares an element's input to the table's cells and sets which cells should be displayed.
 */
function filterProjects() {
    let searchValue = document.querySelector('#queryText').value.trim();
    let displayedArray;
    if (searchValue === "") {
        displayedArray = projArray;
    } else {
        let searchArray = projArray;
        displayedArray = [];
        for (let i = 0; i < searchArray.length; i++) {
            let currentObj = searchArray[i];
            for (let elem in currentObj) {
                if (currentObj[elem] === searchValue) {
                    displayedArray.push(searchArray[i]);
                }
            }
        }
    }
    let table = document.querySelector('#tableRender');
    for (let i = 1; i < table.rows.length; i) {
        table.deleteRow(1);
    }
    for (let j = 0; j < displayedArray.length; j++) {
        updateProjectsTable(displayedArray[j] , j+1);
        makeDelete();
        makeEdit();
    }
    document.querySelector('#generatedTable').childNodes[1].innerHTML = `There are ${displayedArray.length} projects matching this search.`
}