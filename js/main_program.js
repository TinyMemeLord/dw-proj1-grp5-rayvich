'use strict'

//global variables
let regex_patterns=[{input:'proj',pattern: /^.{2,4}$/},
                    {input:'name',pattern: /^[A-Z].{1,60}$/},
                    {input:'title',pattern: /^.{2,50}$/},
                    {input:'category',pattern: /^[A-Z a-z]+/},
                    {input:'hours', pattern: /^[0-9\.]+$/},
                    {input:'rate',pattern: /^[0-9\.]+$/},
                    {input:'status',pattern: /^[A-Za-z]+/}, 
                    {input:'desc',pattern: /^.{0,200}$/}             
                ];
let regex_patterns_confirmed=[{input:'proj',valid: false},
                             {input:'name',valid: false},
                             {input:'title',valid: false},
                             {input:'category',valid:false},
                             {input:'hours',valid:false},
                             {input:'rate',valid:false},
                             {input:'status',valid:false},
                             {input:'desc', valid: true}
                ];
let projArray = [];
document.addEventListener('DOMContentLoaded', init);

/**
 * -Both
 * this function begins the rest of the javascript functions after the DOM content has loaded
 */
function init() {
    setEventListeners();
    setProjArray();
}

/**
 * -Shay Alex Lelichev
 * This function sets ProjArray based on some checks with local storage and session storage and enables/disables some buttons.
 */
function setProjArray() {
    if (sessionStorage.getItem('currentSession') == "[]" || sessionStorage.getItem('currentSession') == null) {
        enable_disable_button('#localAppend' , true);
        projArray = [];
    } else {
        projArray = JSON.parse(sessionStorage.getItem('currentSession'));
    }
    for (let i = 0; i < projArray.length; i++) {
        updateProjectsTable(projArray[i] , i+1);
        makeDelete();
        makeEdit();
        sessionStorage.setItem('currentSession' , JSON.stringify(projArray));
        clear_form();
    }
    checkWriteButton();
    document.querySelector('#generatedTable').childNodes[1].innerHTML = `There are ${JSON.parse(localStorage.getItem('savedProjects')).length} projects in local storage.`
    if (localStorage.getItem('savedProjects') == "[]") {
        enable_disable_button('#localClear' , true);
    } else if (localStorage.getItem('savedProjects') == JSON.stringify(projArray)) {
        enable_disable_button('#localRead' , true);
    } else {
        enable_disable_button('#localClear' , false);
    }
}

/**
 * -Both
 * sets the event listeners of the webpage
 */
function setEventListeners() {
    let inputs = document.querySelectorAll('.top_input');
    inputs.forEach(function(elem){
        elem.addEventListener('input', validate);
    }
    );

    document.querySelector('#add').addEventListener('click' , () => {
        let nbProj = projArray.length;
        let currentProj = createProjectObject(inputs);
        projArray[nbProj] = currentProj;
        updateProjectsTable(currentProj , projArray.length);
        makeDelete();
        makeEdit();
        enable_disable_button('#localAppend' , false);
        sessionStorage.setItem('currentSession' , JSON.stringify(projArray));
        clear_form();
    });
    reset.addEventListener('click', () => {
        clear_form();
        projArray = sessionStorage.getItem('currentSession')
    });
    enable_disable_button('#localWrite' , true);
    checkWriteButton();
    document.querySelector('#localWrite').addEventListener('click' , saveAllProjects2Storage);
    document.querySelector('#localClear').addEventListener('click' , clearAllProjectsFromStorage);
    document.querySelector('#localAppend').addEventListener('click' , appendAllProjects2Storage);
    document.querySelector('#localRead').addEventListener('click' , readAllProjectsFromStorage);
    document.querySelector('#queryText').addEventListener('input' , filterProjects);
}